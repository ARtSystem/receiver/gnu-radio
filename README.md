# GNU Radio

Installation of the [gr-osmosdr](https://github.com/osmocom/gr-osmosdr) package is necessary for the GNURadio flowgraph to properly work. Follow the installtion instructions in the link above. Apart from this package, this should work out of the box. Keep in mind through that the drivers for the BladeRF SDR must be installed. An installtion guide for the BladeRF drivers can be found [here](https://github.com/Nuand/bladeRF/wiki/Getting-Started:-Linux).

